package com.restassured.resources;

public class Payload {

public static String createUserJson(String email, String password) {
    String payload = "{\"email\":\"" + email + "\"," +
    "\"password\":\"" + password + "\"," +
    "\"passwordRepeat\":\"password\"," +
    "\"securityQuestion\":{  \"id\":1," +
                  "\"question\":\"Your eldest siblings middle name?\"," +
                  "\"createdAt\":\"2020-06-03T14:06:01.000Z\"," +
                  "\"updatedAt\":\"2020-06-03T14:06:01.000Z\"" +
                  "}," +
    "\"securityAnswer\":\"asdf\"" +
 "}" ;
 return payload;
}

public static String logineUserJson(String email, String password) {
    String payload = "{\"email\":\""+email+"\",\"password\":\""+password+"\"}" ;
 return payload;
}

public static String submitReviewJson(String email) {
    String payload = "{\"message\":\"Nice product\",\"author\":\"" + email  + "\"}" ;
 return payload;
}

}