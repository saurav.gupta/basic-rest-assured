package com.restassured.tests;

import org.testng.*;
import org.testng.annotations.Test;

import com.github.javafaker.service.FakeValuesService;
import com.github.javafaker.service.RandomService;
import com.restassured.resources.*;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import static io.restassured.RestAssured.given;

import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class JuiceShopTest {

        String url = "http://localhost:3000/";
        String email = generateRandomEmail();
        String password = "password";
        String authSession;
        int rating = 5;
        String desc = "Submit the feedback (anonymous)";

    //@Test(timeOut = 5000)
    public void userFlow() {
            createCustomer();
            loginCustomer();
            listAllProducts();
            submitReview();
    }

    @Test(timeOut = 5000, priority=1)
    public void createCustomer() {
        RestAssured.baseURI = url;
        Response response = given().
                                header("Content-Type", "application/json").
                                body(Payload.createUserJson(email,password)).log().all().
			     when().
				post("api/Users").
		             then().
				assertThat().statusCode(201).log().all().
                             extract().
                                response();

        JsonPath respJson = new JsonPath(response.asString());

        Assert.assertEquals(email, respJson.get("data.email"));
     }

    @Test(timeOut = 5000, priority=2)
    public void loginCustomer() {

        RestAssured.baseURI = url;
        Response response = given().
                                header("Content-Type", "application/json").
                                body(Payload.logineUserJson(email,password)).log().all().
			     when().
				post("rest/user/login").
		             then().
				assertThat().statusCode(200).log().all().
                             extract().
                                response();

        JsonPath respJson = new JsonPath(response.asString());

        authSession = respJson.get("authentication.token");
        Assert.assertEquals(email, respJson.get("authentication.umail"));
    }

    @Test(timeOut = 5000, priority=3)
    public void listAllProducts() {

        RestAssured.baseURI = url;
        Response response = given().
                                header("Accept", "application/json").
                                header("Authorization", "Bearer "+authSession).
                                queryParam("q", "").log().all().
                             when().
				get("rest/products/search").
		             then().
				assertThat().statusCode(200).log().all().
                             extract().
                                response();

        JsonPath respJson = new JsonPath(response.asString());

        Assert.assertEquals("Apple Pomace", respJson.get("data[1].name"));
    }

    @Test(timeOut = 5000, priority=4)
    public void submitReview() {

        RestAssured.baseURI = url;
        Response response = given().
                                header("Content-Type", "application/json").
                                header("Authorization", "Bearer "+authSession).
                                body(Payload.submitReviewJson(email)).log().all().
                             when().
				put("rest/products/1/reviews").
		             then().
				assertThat().statusCode(201).log().all().
                             extract().
                                response();

        JsonPath respJson = new JsonPath(response.asString());

        Assert.assertEquals("success", respJson.get("staus"));
    }
    
    public String generateRandomEmail() {
	FakeValuesService fakeValuesService = new FakeValuesService(new Locale("en-GB"), new RandomService());

	String emailGen = fakeValuesService.bothify("????##@gmail.com");
	Matcher emailMatcher = Pattern.compile("\\w{4}\\d{2}@gmail.com").matcher(emailGen);
	
	Assert.assertTrue(emailMatcher.find());
	return emailGen;
    }
    
}
